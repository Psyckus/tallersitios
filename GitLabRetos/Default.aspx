﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GitLabRetos._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <h1 class="text-center">Retos</h1>
        <h2 class="text-center">El taller completo es: realizar un muro de perfil.</h2>
        <h2 class="text-center">Consiste en 3 retos: </h2>
        <h2 class="text-center">1.Realizar un encabezado para lo cual tendrá que tomar <br> su nombre de la base de datos y mostrarlo.</h2>
        <h2 class="text-center">Puede decorarlo a su gusto</h2>
        <h2 class="text-center">2. Utilizando las herramientas de .Net debera crear un <br> espacio donde los demás puedan dejar comentarios.</h2>
        <h2 class="text-center">Se necesita recibir el nombre de quien escribe el comentario <br> y el comentario realizado. </h2>
        <h2 class="text-center">Puede utilizar dos asp:button</h2>
        <h2 class="text-center">Y luego debera colocar un asp:button y asociarlo a un evento para </h2>
        <h2 class="text-center">guardar los datos recibidos en la base de datos.</h2>
        <h2 class="text-center">3. Deberá conformar un espacio para permitir observar los comentarios.</h2>
        <h2 class="text-center">Para mostrar los comentarios de manera sencilla utilizaremos</h2>
        <h2 class="text-center">un gridview de asp.net, aprovecharemos la propiedad <br> source para cargar los datos provenientes de la base de datos.</h2>
        <h2 class="text-center">Nota: La conexión de base de datos es por medio de un host.</h2>
        <h2 class="text-center">Los metodos necesarios papra comunicarse con la base de datos ya estan diseñados</h2>
        <h2 class="text-center">en la clase estudiante del proyecto BLL</h2>
    </div>

</asp:Content>
